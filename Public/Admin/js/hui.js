function keepState() {
	var clickMenu = $.cookie('current');
	$('.Hui-aside dl dt').each(function(i) {
		if(i == clickMenu) {
			$('.Hui-aside dl dt').eq(i).next().css('display', 'block');
		}
		$(this).click(function() {
			if($(this).next().css('display') == 'none') {
				$('.Hui-aside dl dt').next().slideUp('slow');
				$(this).next().slideDown('slow');
				$.cookie('current', i);
			} else {
				$(this).next().slideUp('slow');
			}
		});
	});
}