$(function(){
	
})

function login(){
	//1.获取用户名、密码、验证码
	var name = $("#form-username").val();
	var password = $("#form-password").val();
	var captcha = $("#form-captcha").val();
	//2.0对用户、密码、验证码进行合法性检测
	if(name==null||name.length==0){
		alert("请输入用户名");
		return false;
	}
	if(name.length>15||name.length<4){
		alert("用户名长度为4—15位");
		return false;
	}	
	if(password==null||password.length==0){
		alert("请输入密码");
		return false;
	}
	if(password.length>15||password.length<5){
		alert("密码长度为5—15位");
		return false;
	}
	if(captcha==null||captcha.length==0){
		alert("请输入验证码")
		return false;
	}
	return true;
}