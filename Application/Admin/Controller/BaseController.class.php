<?php
namespace Admin\Controller;
use Think\Controller;
class BaseController extends Controller{
    public $Config = array();
    public function __construct(){
        parent::__construct();
        if(empty(session('user'))){
            $this->redirect('Login/index');
        }
        $this->initConfig();
    }
    
    public function _empty(){
        $this->error("<h1>访问不合法</h1>",U('Index/index'),3);
    }

    private function initConfig()
    {
        $result = M('Config')->select();
        $config = array();
        foreach ($result as $key => $val) {
            $config[$val['key']] = $val['val'];
        }
        $this->Config = $config;
        $this->assign('config', $config);
    }
}
