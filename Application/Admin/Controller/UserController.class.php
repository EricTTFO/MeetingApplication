<?php
namespace Admin\Controller;
class UserController extends BaseController{ 
    
    public function userIndex(){
        $this->display();
    }
    
    
    /**
     * 获取用户明细信息;
     */
    public function getUserList(){
        $model = M('admin');
        $currPage = I('post.currPage');
        $username = I('post.username');
        $pageRows = 18;
        $totalCount = $model->count();
        $totalPages = ceil($totalCount/$pageRows);
        if(empty($username)){
            $list = $model->order('add_time desc')->limit(($currPage-1)*$pageRows,18)->select();
        }else{
            $list = $model->where(array('username'=>$username))->
            order('add_time desc')->limit(($currPage-1)*$pageRows,18)->select();
        }
        if($list===false){
            $this->ajaxReturn(array('status'=>'F','msg'=>$model->getError()));
        }        
        $this->ajaxReturn(array('status'=>'S','totalCount'=>$totalCount,'totalPages'=>$totalPages,'data'=>$list));
    }
    
    
    public function userAdd(){
        if(IS_POST){
            if(empty(I('post.username')||empty(I('post.userId')||empty(I('post.password'))||empty(I('post.password2'))))){
                $this->ajaxReturn(array('status'=>'F','msg'=>'信息不完整'));
            }
            if(I('post.password')!==I('post.password2')){
                $this->ajaxReturn(array('status'=>'F','msg'=>'两次密码不相同'));
            }
            $data = array(
                'username'=>I('post.username'),
                'userId'=>I('post.userId'),
                'password'=>get_pwd(I('post.password'),'activity'),
                'user_status'=>1,
                'add_user'=>'admin',//session;
            );
            $model = M('admin');
            $res =$model->add($data);
            if($res===FALSE){
                $this->ajaxReturn(array('status'=>'F','msg'=>$model->getError()));
            }
            $this->ajaxReturn(array('status'=>'S'));
        }
        $this->display();      
    }
    
    public function userDel(){
        $ids = I('post.id');
        if(empty($ids)){
            $this->ajaxReturn(array('status'=>'F','msg'=>'传入用户id不能为空'));
        }
        $idArr = explode(',', $ids);
        $model = M('admin');
        $where['id']=array('in',$idArr);
        $res = $model ->where($where)->delete();
        
        if($res===false){
            $this->ajaxReturn(array('status'=>'F','msg'=>$model->getError()));
        }
        $this->ajaxReturn(array('status'=>'S'));
    }  
    
    //更新密码
    public function userUpdate(){
        if(IS_POST){
            $username=I('post.username');
            $userId = I('post.userId');
            $oldpassword = I('post.oldpassword');
            $newpassword1 = I('post.newpassword1');
            $newpassword2 = I('post.newpassword2');
            if($newpassword1!==$newpassword2){
                $this->ajaxReturn(array('status'=>'F','msg'=>'新密码不相同'));
            }
            $model = M('admin');
            $user = $model->where(array('username'=>$username,'userId'=>$userId))->find();
            if($user['password']!==get_pwd($oldpassword, 'activity')){
                $this->ajaxReturn(array('status'=>'F','msg'=>'原密码输入不正确'));
            }
            $res=$model->where(array('username'=>$username,'userId'=>$userId))->save(array('password'=>get_pwd($newpassword2,'activity')));
            if($res===false){
                $this->ajaxReturn(array('status'=>'F','msg'=>$model->getError()));
            }
            $this->ajaxReturn(array('status'=>'S'));
        }
        $this->display();
    }
    
    public function updateStatus(){        
        $id = I('post.id');
        $status = I('post.status');
        $model = M('admin');     
        if($status==1){
            $status=0;
        }else{
            $status=1;
        }
        $res = $model->where(array('id'=>$id))->save(array('user_status'=>$status));
        if($res===false){
            $this->ajaxReturn(array('status'=>'F','msg'=>$model->getError()));
        }
        $this->ajaxReturn(array('status'=>'S'));
    }
    
    
    
    
}