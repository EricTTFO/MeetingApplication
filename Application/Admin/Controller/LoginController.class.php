<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Verify;

class LoginController extends Controller{
    public function index(){

        $result = M('Config')->select();
        $config = array();
        foreach ($result as $key => $val) {
            $config[$val['key']] = $val['val'];
        }
        $this->assign('config', $config);
        $this->display();
    }
    
    //用户登陆
    public function login(){
        $username = I("username");
        $password = I("password");
        $captcha = I("post.captcha");
        if(empty($username)){
            $this->ajaxReturn(array('status'=>'F','msg'=>'用户名不能为空'));
        }
        if(empty($password)){
            $this->ajaxReturn(array('status'=>'F','msg'=>'密码不能为空'));
        }
        if(empty($captcha)){
            $this->ajaxReturn(array('status'=>'F','msg'=>'验证码不能为空'));
        }        
        $verify = new \Think\Verify();
        if(!$verify->check($captcha)){
            $this->ajaxReturn(array('status'=>'F','msg'=>'验证码不正确'));
        }        
        $user = M('admin')->where(array('username'=>$username,'user_status'=>1))->find();
        if(empty($user)){
            $this->ajaxReturn(array('status'=>'F','msg'=>'该用户不存在'));
        }        
        if(get_pwd($password,'activity')!=$user['password']){
            $this->ajaxReturn(array('status'=>'F','msg'=>'密码不正确'));
        }
        $id = $user['id'];        
        $data['username']=$username;
        $data['login_count']=$user['login_count']+1;
        $data['last_login_time']=date('Y-m-d H:i:s',time());
        $data['account_status']=1;
        $result = M('admin')->where("id=$id")->save($data);
        if($result===false){
            $this->ajaxReturn(array('status'=>'F','msg'=>'Login Fail'));
            return ;
        }
        session('user',$user);
        $this->ajaxReturn(array('status'=>'S'));
    }
    
    //产生验证码
    public function getCaptcha(){
        $verify = new Verify();
        $verify->fontSize = 25;
        $verify->length   = 4;
        $verify->codeSet = '0123456789';
        $verify->imageW = 165;
        $verify->imageH = 48;
        $verify->useCurve = false;        
        $verify->useNoise = false;//false;
        $verify->entry();
    }
    
    //退出登陆
    public function logout(){
        M('admin')->where(array('username'=>session('user')['username']))->save(array('account_status'=>0));        
        session('user',null);
        $this->redirect('Login/index');
    }
    
}