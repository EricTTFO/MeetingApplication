<?php

namespace Admin\Controller;
class SystemController extends BaseController
{
    public function siteIndex()
    {
        S('siteInfo', $this->Config);

        $this->display();
    }

    public function updateSite()
    {
        define('DS', DIRECTORY_SEPARATOR);
        define('DESTINATION', "Public" . DS . "Upload" . DS . "site" . DS);

        $data = $_POST;
        if (!empty($_FILES['logo']['name'])) {
            $fileName = 'logo.png';
            //验证上传文件的类型及大小
            $type = $_FILES['logo']['type'];
            $size = $_FILES['logo']['size'];
            if (!in_array($type, C('allowedExts'))) {
                $this->ajaxReturn(array('status' => 'F', 'msg' => '图片类型不正确'));
            }

            if ($size > C('imageSize')) {
                $this->ajaxReturn(array('status' => 'F', 'msg' => '活动图片不能超过' . (C('imageSize') / 1024) . 'KB'));
            }

            $result = move_uploaded_file($_FILES['logo']['tmp_name'], DESTINATION . $fileName);
            if ($result === false) {
                $this->ajaxReturn(array('status' => 'F', 'msg' => '上传图片失败'));
            }
            $data['logo'] = "/Public/Upload/Site/" . $fileName;
        }
        $new_data = array();
        foreach ($data as $key => $val) {
            $new_data[] = ['key'=>$key, 'val'=>$val];
        }
        M('Config')->addAll($new_data, array(), true);

        $this->ajaxReturn(array('status' => 'S'));
    }
}