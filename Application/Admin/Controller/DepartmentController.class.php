<?php

namespace Admin\Controller;
class DepartmentController extends BaseController
{

    private $tbName = 'department';

    public function index()
    {
        $this->display();
    }

    public function pageList()
    {
        $model = M($this->tbName);
        $row = I('post.row');
        $currPage = I('post.currPage');
        $name = I('post.name');
        $where = !empty($name) ? ['name' => $name] : array();
        $totalCount = $model->where($where)->count();
        $totalPages = ceil($totalCount / $row);
        $list = $model->where($where)->order('sorting asc')->limit(($currPage - 1) * $row, $row)->select();
        if ($list === false) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => $model->getError()));
        }
        $this->ajaxReturn(array('status' => 'S', 'totalCount' => $totalCount, 'totalPages' => $totalPages, 'data' => $list));
    }

    public function add()
    {
        $id = I('get.id');
        $info = array();
        if ($id){
            $info = M($this->tbName)->find($id);
        }
        $this->assign('info', $info);
        $this->display();
    }

    public function save()
    {
        $data = I('post.');
        if (empty($data['name'])) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => '部门名称不能为空'));
        }
        if (empty($data['id'])) {
            $data['operator'] = session('user')['username'];
            $data['crdate'] = date('Y-m-d H:i:s');
        }
        $model = M($this->tbName);
        $res = $model->add($data, array(), true);
        if ($res === FALSE) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => $model->getError()));
        }
        $this->ajaxReturn(array('status' => 'S'));
    }

    public function del()
    {
        $ids = I('post.id');
        if (empty($ids)) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => '传入id不能为空'));
        }
        $idArr = explode(',', $ids);
        $model = M($this->tbName);
        $where['id'] = array('in', $idArr);
        $res = $model->where($where)->delete();

        if ($res === false) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => $model->getError()));
        }
        $this->ajaxReturn(array('status' => 'S'));
    }
}