<?php
namespace Admin\Controller;
class IndexController extends BaseController{    
    //展示后台首页
    public function index(){
        $this->display();
    }
    
    public function getInfo(){
        $tbName = 'apply';
        $model = new \Think\Model();
        $sql = '(SELECT count(1) as total, "title",'.
            ' count(CASE `status` WHEN 1 THEN 1 END ) AS status1,'.
            ' count(CASE `status` WHEN 2 THEN 1 END ) AS status2,'.
            ' count(CASE `status` WHEN 3 THEN 1 END ) AS status3'.
            ' FROM '.$tbName.')'.
            ' union all'.
            ' (SELECT count(1) as total_d, "今日",'.
            ' count(CASE `status` WHEN 1 THEN 1 END ) AS status1,'.
            ' count(CASE `status` WHEN 2 THEN 1 END ) AS status2,'.
            ' count(CASE `status` WHEN 3 THEN 1 END ) AS status3'.
            ' FROM '.$tbName.' Where to_days(`crdate`) = to_days(now()))'.
            ' union all'.
            ' (SELECT count(1) as total_w, "本周",'.
            ' count(CASE `status` WHEN 1 THEN 1 END ) AS status1,'.
            ' count(CASE `status` WHEN 2 THEN 1 END ) AS status2,'.
            ' count(CASE `status` WHEN 3 THEN 1 END ) AS status3'.
            ' FROM '.$tbName.' Where YEARWEEK(date_format(`crdate`,"%Y-%m-%d")) = YEARWEEK(now()))'.
            ' union all'.
            ' (SELECT count(1) as total_m, "本月",'.
            ' count(CASE `status` WHEN 1 THEN 1 END ) AS status1,'.
            ' count(CASE `status` WHEN 2 THEN 1 END ) AS status2,'.
            ' count(CASE `status` WHEN 3 THEN 1 END ) AS status3'.
            ' FROM '.$tbName.' Where DATE_FORMAT(`crdate`, "%Y%m" ) = DATE_FORMAT( CURDATE() , "%Y%m" ))'.
            ';';
        $result = $model->query($sql);
        if($result===false){
            $this->ajaxReturn(array('status'=>'F'));
        }
        $this->ajaxReturn(array('status'=>'S','data'=>$result));
    }
   
}