<?php

namespace Admin\Controller;
class ApplyController extends BaseController
{

    private $tbName = 'apply';

    public function index()
    {
        //使用场地
        $place_list = explode('|', $this->Config['meet_place']);
        $this->assign('place_list', $place_list);

        $this->display();
    }

    public function pageList()
    {
        $model = M($this->tbName);
        $row = I('post.row');
        $currPage = I('post.currPage', 1);
        $applyer = I('post.applyer');
        $department = I('post.department');
        $status = I('post.status');
        $place = I('post.place');

        $where = array();
        if ($department) {
            $where['department'] = trim($department);
        }
        if ($status) {
            $where['status'] = $status;
        }
        if ($place) {
            $where['place'] = $place;
        }
        if ($applyer) {
            $where['applyer'] = trim($applyer);
        }

        $totalCount = $model->where($where)->count();
        $totalPages = ceil($totalCount / $row);
        $list = $model->where($where)->order('crdate desc')->limit(($currPage - 1) * $row, $row)->select();
        if ($list === false) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => $model->getError()));
        }
        $this->ajaxReturn(array('status' => 'S', 'totalCount' => $totalCount, 'totalPages' => $totalPages, 'data' => $list));
    }

    public function del()
    {
        $ids = I('post.id');
        if (empty($ids)) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => '传入id不能为空'));
        }
        $idArr = explode(',', $ids);
        $model = M($this->tbName);
        $where['id'] = array('in', $idArr);
        $res = $model->where($where)->delete();

        if ($res === false) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => $model->getError()));
        }
        $this->ajaxReturn(array('status' => 'S'));
    }

    public function updateStatus()
    {
        $id = I('post.id');
        $status = I('post.status');
        $remark = I('post.remark');
        if (empty($id) || empty($status)) {
            $this->ajaxReturn(array('status' => 'F'));
        }
        $model = M($this->tbName);
        $where = ['status' => 1, 'id' => $id];
        $res = $model->where($where)->save([
            'status' => $status,
            'operator' => session('user')['username'],
            'remark' => $remark,
            'tstamp'=>date('Y-m-d H:i:s')
        ]);
        if ($res === false) {
            $this->ajaxReturn(array('status' => 'F'));
        }
        $this->ajaxReturn(array('status' => 'S'));
    }
}