<?php
function get_pwd($pass,$salt){
	$password = md5($pass.'#'.substr(md5($salt),20));
	return $password;
}

/**
 * 导出excel
 * @param array $data 导入数据
 * @param string $savefile 导出excel文件名
 * @param array $fileheader excel的表头
 * @param string $sheetname sheet的标题名
 */
function export($data, $savefile, $fileheader, $sheetname){
    import("Org.Util.PHPExcel");
    import("Org.Util.PHPExcel.Reader.Excel2007");
    $excel = new \PHPExcel();
    
    //防止中文命名，下载时ie9及其他情况下的文件名称乱码
    if (is_null($savefile)) {
        $savefile = time();
    }else{
        iconv('UTF-8', 'GB2312', $savefile);
    }
    //设置excel属性
    $objActSheet = $excel->getActiveSheet();
    //根据有生成的excel多少列，$letter长度要大于等于这个值
    $letter = array('A','B','C','D','E','F','F','G');
    //设置当前的sheet
    $excel->setActiveSheetIndex(0);
    //设置sheet的name
    $objActSheet->setTitle($sheetname);
    //设置表头
    for($i = 0;$i < count($fileheader);$i++) {
        //单元宽度自适应,1.8.1版本phpexcel中文支持勉强可以，自适应后单独设置宽度无效
        $objActSheet->getColumnDimension("$letter[$i]")->setWidth(19);
        //设置表头值，这里的setCellValue第二个参数不能使用iconv，否则excel中显示false
        $objActSheet->setCellValue("$letter[$i]1",$fileheader[$i]);
        //设置表头字体样式
        $objActSheet->getStyle("$letter[$i]1")->getFont()->setName('微软雅黑');
        //设置表头字体大小
        $objActSheet->getStyle("$letter[$i]1")->getFont()->setSize(12);
        //设置表头字体是否加粗
        $objActSheet->getStyle("$letter[$i]1")->getFont()->setBold(true);
        //设置表头文字垂直居中
        $objActSheet->getStyle("$letter[$i]1")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //设置文字上下居中
        $objActSheet->getStyle($letter[$i])->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //设置表头外的文字垂直居中
        $excel->setActiveSheetIndex(0)->getStyle($letter[$i])->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    }
    //单独设置D列宽度为15
    //$objActSheet->getColumnDimension('D')->setWidth(50);
    //这里$i初始值设置为2，$j初始值设置为0，自己体会原因
    for ($i = 2;$i <= count($data) + 1;$i++) {
        $j = 0;
        foreach ($data[$i - 2] as $key=>$value) {
            $objActSheet->setCellValue("$letter[$j]$i",$value);
            $j++;
        }
        //设置单元格高度，暂时没有找到统一设置高度方法
        $objActSheet->getRowDimension($i)->setRowHeight('20px');
    }
    $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    header ( 'Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
    header ( 'Content-Disposition:attachment;filename="' ."$savefile.xlsx". $filename . '"' );
    header ( 'Cache-Control:max-age=0' );
    $objWriter->save("php://output");
}


function read($filename,$encode,$file_type)
{
    import("Org.Util.PHPExcel");
    import("Org.Util.PHPExcel.IOFactory");
    if (strtolower($file_type) == 'xls')//判断excel表类型为2003还是2007
    {
        import("Org.Util.PHPExcel.Reader.Excel5");
        $objReader = \PHPExcel_IOFactory::createReader('Excel5');
    } elseif (strtolower($file_type) == 'xlsx') {
        import("Org.Util.PHPExcel.Reader.Excel2007");
        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
    }
    $objReader->setReadDataOnly(true);
    
    $objPHPExcel = $objReader->load($filename);
    
    $objWorksheet = $objPHPExcel->getActiveSheet();
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();
    $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
    $excelData = array();
    for ($row = 1; $row <= $highestRow; $row++) {
        for ($col = 0; $col < $highestColumnIndex; $col++) {
            $excelData[$row][] = (string)$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
        }
    }
    return $excelData;
}