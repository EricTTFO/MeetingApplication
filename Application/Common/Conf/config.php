<?php
return array(
    'DB_TYPE'               =>  'mysql',     // 数据库类型
    'DB_HOST'               =>  'localhost',
    'DB_NAME'               =>  'meeting',          // 数据库名
    'DB_USER'               =>  'root',      // 用户名
    'DB_PWD'                =>  '',          // 密码
    'DB_PORT'               =>  '3306',        // 端口
    'DB_PREFIX'             =>  '',    // 数据库表前缀
    'URL_CASE_INSENSITIVE'  =>  true,  // URL区分大小写
    'allowedExts'=>array('image/gif','image/jpeg','image/jpg','image/pjpeg','image/x-png','image/png'),
    'imageSize'=>409600,
    'DEFAULT_TIMEZONE' => 'Asia/Shanghai',

    'MODULE_ALLOW_LIST'     => array('Home','Admin'),
    'DEFAULT_MODULE'        => 'Home',
    'URL_MODEL'             => '2',
);