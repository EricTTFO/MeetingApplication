<?php

namespace Home\Controller;

use Think\Controller;

class IndexController extends Controller
{
    public function index()
    {
        //使用场地
        $siteInfo = S('siteInfo');
        if (!$siteInfo){
            $siteInfo = $this->initConfig();
        }
        $place = explode('|', $siteInfo['meet_place']);
        $this->assign('place', $place);
        $this->assign('config', $siteInfo);

        $this->display();
    }

    public function history()
    {
        $this->display();
    }

    public function apply()
    {
        $data = I('post.');
        $data['crdate'] = date('Y-m-d H:i:s');
        $res = M('apply')->add($data);
        if (!$res){
            $this->ajaxReturn(array('status' => 'F', 'msg' => '申请失败，请刷新重试'));
        }
        $this->ajaxReturn(array('status' => 'S', 'msg' => '申请成功'));
    }

    public function query()
    {
        $data = I('post.');
        if (!$data['applyer']) {
            $this->ajaxReturn(array('status' => 'F', 'msg' => '请填写申请人查询记录'));
        }
        $where['applyer'] = $data['applyer'];
        if ($data['crdate']){
            $where['crdate'] = ['gt', $data['crdate']];
        }
        $list = M('apply')->where($where)->order('crdate desc')->select();

        $this->ajaxReturn(array('status' => 'S', 'data' => $list));
    }

    private function initConfig()
    {
        $result = M('Config')->select();
        $config = array();
        foreach ($result as $key => $val) {
            $config[$val['key']] = $val['val'];
        }
        S('siteInfo', $config);
        return $config;
    }
}